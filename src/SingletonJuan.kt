
object SingletonJuan
{

    init {
        println("Juan el trabajador invoked.")
    }
    var job = "Trabajo de Juan"
    fun printName()
    {
        println(job)
    }

}

fun main(args: Array<String>) {
    SingletonJuan.printName()
    SingletonJuan.job = "Levanta un muro"
    var trabajo1 = JuanConstructor()
    SingletonJuan.job = "Cocinar unas carnitas"
    var trabajo2 = JuanCocinero()
}

class JuanConstructor {

    init {
        println("El trabajo que realiza juan ahora es : ${SingletonJuan.job}")
        SingletonJuan.printName()
    }
}

class JuanCocinero {

    init {
        println("El trabajo que realiza juan ahora es : ${SingletonJuan.job}")
        SingletonJuan.printName()
    }

}